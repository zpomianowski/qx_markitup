/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("markitup.demo.theme.Theme",
{
  meta :
  {
    color : markitup.demo.theme.Color,
    decoration : markitup.demo.theme.Decoration,
    font : markitup.demo.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : markitup.demo.theme.Appearance
  }
});