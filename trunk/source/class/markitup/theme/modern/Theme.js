/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("markitup.theme.modern.Theme",
{
  meta :
  {
    color : markitup.theme.modern.Color,
    decoration : markitup.theme.modern.Decoration,
    font : markitup.theme.modern.Font,
    appearance : markitup.theme.modern.Appearance,
    icon : qx.theme.icon.Tango
  }
});