/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("markitup.theme.classic.Theme",
{
  meta :
  {
    color : markitup.theme.classic.Color,
    decoration : markitup.theme.classic.Decoration,
    font : markitup.theme.classic.Font,
    appearance : markitup.theme.classic.Appearance,
    icon : qx.theme.icon.Oxygen
  }
});