/* ************************************************************************

Copyright:
 2009 ACME Corporation -or- Your Name, http://www.example.com

License:
 LGPL: http://www.gnu.org/licenses/lgpl.html
 EPL: http://www.eclipse.org/org/documents/epl-v10.php
 See the LICENSE file in the project's top-level directory for details.

Authors:
 * Zbigniew Pomianowski (zbhp)

 ************************************************************************ */

/**
* This is the main class of contribution "markitup"
*
* TODO: Replace the sample code of a custom button with the actual code of
* your contribution.
*
* @asset(markitup/*)
*/

qx.Class.define("markitup.Editor",
{
    extend : qx.ui.tabview.TabView,

    include: [qx.ui.form.MForm],
    implement: [qx.ui.form.IForm, qx.ui.form.IStringForm],

    properties : {
        value :
        {
            check : "String",
            nullable : true,
            event: "changeValue",
            apply: "_applyValue",
            init : null
        }
    },

    statics: {
        // not empty validator
        notEmpty: function() {
            return function(value) {
                var errorMessage = qx.locale.Manager.tr("Cannot be empty!");
                qx.util.Validate.checkString(value, null, errorMessage);
                if (value.length == 0)
                    throw new qx.core.ValidationError("Validation Error", errorMessage);
            }
        }
    },

    construct : function(uri, edition_first)
    {
        this.__fu = uri;
        this.base(arguments);
        this.setBarPosition("bottom");
        this.setAppearance("markitup");

        var page1 = new qx.ui.tabview.Page(this.tr("Editor"));
        page1.setLayout(new qx.ui.layout.VBox());
        this.__editor = this._createChildControl("editor");
        page1.add(this.__editor, { flex: 1 });
        this.add(page1);

        var page2 = new qx.ui.tabview.Page(this.tr("View"));
        page2.setLayout(new qx.ui.layout.VBox());
        this.__html = this._createChildControl("preview");
        page2.add(this.__html, { flex: 1 });
        this.add(page2);
        if (edition_first) this.setSelection([page1]);
        else this.setSelection([page2]);

        this.setupBindings();
    },

    members: {
        __fu: null,
        __editor: null,
        __html: null,

        getEditor: function() {
            return this.__editor;
        },

        _createChildControlImpl : function(id) {
            var control;
            switch(id) {
                case "editor":
                    control = new qx.ui.form.TextArea();
                break;

                case "preview":
                    control = new qx.ui.embed.Html().set({
                        overflowY: "auto",
                        appearance: "textarea",
                        nativeContextMenu: true
                    });
                break;
            }
            return control || this.base(arguments, id);
        },

        setRequired: function(value) {
            this.__html.setRequired(value);
        },

        setReadOnlyMode: function(value) {
            this.__editor.setEnabled(!value);
        },

        setupBindings: function() {
            var that = this;
            var editor = this.__editor;
            var html = this.__html;

            editor.bind("value", this, "value");
            editor.bind("value", html, "html", {
                converter: function(text) {
                    if (!text) return "";
                    var response = "";
                    var req = new qx.io.request.Xhr(that.__fu, "POST");
                    req.setAsync(false);
                    req.setRequestData({ content: text });
                    req.setTimeout(5000);
                    req.addListener("success", function(e) {
                        var req = e.getTarget();
                        var data = req.getResponse();
                        response = data.content || "";
                    }, this);
                    req.send();
                    return response;
                }
            });
        },

        _applyValue: function(newValue, oldValue) {
            this.__editor.setValue(newValue);
        }
    }
});
